package ru.baulina.tm.api.repository;

import ru.baulina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(Long id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(Long id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
