package ru.baulina.tm.controller;

import ru.baulina.tm.api.controller.IProjectController;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.model.Project;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final  String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
        System.out.println();
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    private  void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final Long id  = TerminalUtil.nexLong();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateTaskById(id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index  = TerminalUtil.nexInt() -1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description  = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateTaskByIndex(index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final Long id = TerminalUtil.nexLong();
        final Project project = projectService.removeOneById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nexInt() -1;
        final Project project = projectService.removeOneByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
