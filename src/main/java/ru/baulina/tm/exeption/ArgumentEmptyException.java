package ru.baulina.tm.exeption;

public class ArgumentEmptyException extends RuntimeException {

    public ArgumentEmptyException() {
        super("Error! Argument is empty...");
    }

}
