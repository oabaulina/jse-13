package ru.baulina.tm.exeption;

public class IdEmptyException extends RuntimeException{

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
