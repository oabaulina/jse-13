package ru.baulina.tm.exeption;

public class CommandEmptyException extends RuntimeException{

    public CommandEmptyException() {
        super("Error! Command is empty...");
    }

}
