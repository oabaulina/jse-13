package ru.baulina.tm.exeption;

public class CommandIncorrectException extends RuntimeException{

    public CommandIncorrectException() {
        super("Error! Command not exist...");
    }

}
