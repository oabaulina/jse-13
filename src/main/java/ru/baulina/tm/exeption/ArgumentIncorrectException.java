package ru.baulina.tm.exeption;

public class ArgumentIncorrectException extends RuntimeException {

    public ArgumentIncorrectException() {
        super("Error! Argument not exist...");
    }

}
