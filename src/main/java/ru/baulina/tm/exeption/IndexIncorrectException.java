package ru.baulina.tm.exeption;

public class IndexIncorrectException extends RuntimeException{

    public IndexIncorrectException(String value) {
        super("Error! This value '" + value + "' is not number...");
    }
    public IndexIncorrectException() {
        super("Error! Index is empty...");
    }

}
