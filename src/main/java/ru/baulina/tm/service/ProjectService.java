package ru.baulina.tm.service;

import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.exeption.IdEmptyException;
import ru.baulina.tm.exeption.IndexIncorrectException;
import ru.baulina.tm.exeption.NameEmptyEcxeption;
import ru.baulina.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);

    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);

    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(Long id) {
        if (id == null || id < 0) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        //if ((index < 0) || (index >= projects.size())) return null;
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(final Long id) {
        if (id == null || id < 0) throw new IdEmptyException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateTaskById(final Long id, final String name, final String description) {
        if (id == null || id < 0) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyEcxeption();
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return null;
    }

}
