package ru.baulina.tm.util;

import ru.baulina.tm.exeption.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nexInt() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }

    }

    static Long nexLong() {
        final String value = nextLine();
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
